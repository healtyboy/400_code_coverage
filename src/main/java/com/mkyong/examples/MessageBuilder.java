package com.mkyong.examples;

public class MessageBuilder {

    public String getMessage(String name) {
    	
        StringBuilder result = new StringBuilder();
        
        if (name != null && name.trim().length() > 0) {
            result.append("Hello " + name);
        } else {
        	result.append("Please provide a name!");
        }
        return result.toString();
    }

}
